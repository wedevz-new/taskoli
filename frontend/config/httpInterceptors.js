(function(){
    'use strict';
    var app = angular.module('taskoli');
    app.factory('HttpInterceptors', HttpInterceptors);

    HttpInterceptors.$inject = ['AuthService'];

    function HttpInterceptors(AuthService) {
            var service = {
                request: request
            };

            function request(config) {
                var token = AuthService.getToken();
                if(token) {
                    config.headers['x-access-token'] = token;
                } 
                return config;
            }
           return service;
        }
})();
