(function(){
    'use strict';
    var app = angular.module('taskoli');
    app.controller('AccountController', AccountController);

    AccountController.$inject = ['$scope', '$window', 'toastr', '$state'];
    function AccountController($scope, $window, toastr, $state) {
        if($state.current.name == 'error')
        {
            toastr.error('Error Logging in', 'Account does not exist')
        }

        $scope.facebookLogin = function () {
            $window.location = $window.location.protocol + '//' + $window.location.host + '/api/account/login/facebook';
        }

         $scope.googleLogin = function () {
            $window.location = $window.location.protocol + '//' + $window.location.host + '/api/account/login/google';
        }        
        

    }
})();
