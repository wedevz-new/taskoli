(function(){
    'use strict';
    var app = angular.module('taskoli');
    app.controller('FacebookController', FacebookController);

    FacebookController.$inject = ['$scope', '$stateParams', 'AccountService', '$state', '$timeout'];

    function FacebookController($scope, $stateParams, AccountService, $state, $timeout)  {
        AccountService.loginFacebook($stateParams.token);
        console.log("Logged in via FB");
        $state.go('login-redirect', {}, { reload: true });
        $timeout(function() {
            $state.go('home', {}, { reload: true });
        }, 2000);
    }
})();
