(function() {
    'usestrict';

    angular.module('taskoli').config(Account);

    Account.$inject = ['$stateProvider', '$urlRouterProvider'];

    function Account($stateProvider, $urlRouterProvider) {
        // States
        $stateProvider
          .state('facebook', {
            parent: 'app',
            url: '/facebook/:token',
            views:{
                'content@':{
                    controller: 'FacebookController'
                }
            }
          })
          .state('login-redirect', {
            parent: 'app',
            url: '/login/redirect',
            views:{
                'content@':{
                    templateUrl: '/frontend/account/view/login-redirect.html'
                }
            }
          })
          .state('logout', {
            parent: 'app',
            url: '/logout',
            views:{
                'content@':{
                    templateUrl: '/frontend/account/view/logout.html'
                }
            }
          });
    }
})();