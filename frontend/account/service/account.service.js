(function(){
    'use strict';
    var app = angular.module('taskoli');
    app.factory('AccountService', AccountService);

    AccountService.$inject = ['$http', 'apiURL', 'AuthService', '$q'];

    function AccountService($http, apiURL, AuthService) {
            var service = {
             getSession: getSession,
             isLoggedIn: isLoggedIn,
             loginFacebook: loginFacebook,
             loginGoogle: loginGoogle,
             logout: logout
            };
            // create user variable
            function getSession() {
                if(isLoggedIn) {
                    return $http.post(apiURL + '/account/auth/me', {});
                }
                else {
                    $q.reject({message: 'User has no token'});
                }
            }

            function isLoggedIn() {
                if(AuthService.getToken()) {
                    return true;
                } else {
                    return false;
                }
            }

            function loginFacebook(token) {
               AuthService.setToken(token);
            }

            function loginGoogle() {
                $http({
                    method: 'GET',
                    url: apiURL + '/account/login/google'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    console.error(response);
                    return response;
                });
            }

            function logout() {
                AuthService.removeToken();
            }

            return service;
        }
})();
