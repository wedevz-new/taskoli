(function () {
    'use strict';
    var app = angular.module('taskoli');
    app.controller('PostTaskController', PostTaskController);

    PostTaskController.$inject = ['$scope', '$sce', 'toastr', 'Upload', '$uibModal', '$uibModalInstance', '$filter', 'PostTaskService', 'siteURL', 'apiURL', 'userData', '$timeout', 'NgMap', '$http', '$window', 'moment', 'googleMapKey'];

    function PostTaskController($scope, $sce, toastr, Upload, $uibModal, $uibModalInstance, $filter, PostTaskService, siteURL, apiURL, userData, $timeout, NgMap, $http, $window, moment, googleMapKey) {
        var timer = 200;

        $scope.user_id = userData.id;


        $scope.task = {
            user_id: userData.id,
            task_description: '',
            task_title: '',
            task_budget: null,
            task_sooner: 1,
            task_location: null,
            img_name: []
        };
        $scope.CurrentDate = new Date();
        $scope.site_url = siteURL;
        $scope.ratingTitle = ['One', 'Two', 'Three', 'Four', 'Five'];
        $scope.message = 'You can not hide. :)';
        $scope.toggle = true;
        $scope.directiontoggle = true;
        $scope.log = '';
        $scope.imag_urls = [];
        $scope.form = {}

        $scope.callbackFunc = function (param) {
            console.log('I know where ' + param + ' are. ' + message);
            console.log('You are at' + $scope.map.getCenter());
            $scope.location_address = $scope.place.formatted_address;
            $scope.data = {
                lat: $scope.place.geometry.location.lat(),
                lng: $scope.place.geometry.location.lng()
            };
        };
        // End ::::::::::

        // Auto Completet Places
        $scope.autocompleteOptions = {
            componentRestrictions: {
                country: 'ca'
            }
        }

        $scope.$on('g-places-autocomplete:select', function (event, place) {
            $scope.data = {
                lat: place.geometry.location.lat(),
                lng: place.geometry.location.lng()
            };
            $scope.map.setCenter(place.geometry.location);
        });

        // Toggle Map on My Task popup
        $scope.toggleMap = function () {
            $scope.toggle = $scope.toggle === false ? true : false;
        };

        $scope.directionToggle = function () {
            $scope.directiontoggle = $scope.directiontoggle === false ? true : false;
        };

        if (!userData) {
            MyTasksService.getTaskByID(userData.id).then(function (res) {
                $scope.tasks = res.data['task'][0];
                $scope.images = res.data['images'];
                $scope.site_url = siteURL;
            }, function (err) {
                console.error(err.statusText + ' at ' + err.config.url);
            });
        }

        $scope.$watch('files', function () {
            $scope.upload($scope.files);
        });

        $scope.$watch('task_img', function () {
            if ($scope.files != null) {
                $scope.files = [$scope.files];

            }
        });

        $scope.upload = function (files) {
            if (files && files.length) {
                for (var i = 0; i < 5; i++) {
                    var file = files[i];
                    if (file) {
                        Upload.upload({
                            url: apiURL + '/upload',
                            data: {
                                file: file
                            }
                        }).then(function (resp) {
                            $timeout(function () {
                                $scope.apiURL = apiURL;
                                $scope.imag_urls.push(resp.data.filename);
                                $scope.log = 'file: ' +
                                    resp.config.data.file.name +
                                    ', Response: ' + JSON.stringify(resp.data) +
                                    '\n' + $scope.log;
                            });
                        }, null, function (evt) {
                            var progressPercentage = parseInt(100.0 *
                                evt.loaded / evt.total);
                            $scope.log = 'progress: ' + progressPercentage +
                                '% ' + evt.config.data.file.name + '\n' +
                                $scope.log;
                        });
                    }
                }
            }
        };
        $scope.setPopupData = function (Task) {
             NgMap.getMap().then(function (map) {
                $scope.map = google.maps.event.trigger(map, "resize");
            });
        };

        $scope.btn_post_task = function (Task) {
            PostTaskService.addTask(Task).then(function (res) {
                toastr.success('Task Posted Successfull', '');
                $uibModalInstance.close();
            }, function (err) {
                console.error(err.statusText + ' at ' + err.config.url);
            });
        };


        $scope.submitForm = function () {
            if ($scope.form.userForm.$valid) {
                console.log('user form is in scope');
                $modalInstance.close('closed');
            } else {
                console.log('userform is not in scope');
            }
        };
    }
})();