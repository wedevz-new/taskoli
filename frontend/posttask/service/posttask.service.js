(function() {
'use strict';
    var app = angular.module('taskoli');
   
   app.service('PostTaskService', PostTaskService);

    PostTaskService.inject = ['$http', '$q', 'apiURL'];
    function PostTaskService($http, $q, apiURL) {
        var service = {
            addTask: addTask
        };
        
        function addTask(TaskData) { 
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: apiURL + '/addtask/', 
                    data: TaskData
                    }).then(function successCallback(response) {
                        deferred.resolve(response);
                    }, function errorCallback(message, code) {
                        deferred.reject(message);
                    });
                    return deferred.promise;
        }

        return service;
    }
})();