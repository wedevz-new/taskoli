(function () {
    'use strict';
    var app = angular.module('taskoli');
    app.controller('MyTasksController', MyTasksController);

    MyTasksController.$inject = ['$scope', '$sce', 'toastr', 'Upload', '$uibModal', '$uibModalInstance', '$filter', 'MyTasksService', 'siteURL', 'apiURL', 'param', '$timeout', 'NgMap', '$http', '$window', 'moment', 'googleMapKey'];

    function MyTasksController($scope, $sce, toastr, Upload, $uibModal, $uibModalInstance, $filter, MyTasksService, siteURL, apiURL, param, $timeout, NgMap, $http, $window, moment, googleMapKey) {
        var timer = 200;
        $scope.user_id = param.userid;
        $scope.googleMapsUrl = "https://maps.googleapis.com/maps/api/js?key=" + googleMapKey;

        $scope.employer_search = {
            detailed_search: '',
            task_status: 'all',
            task_sort: 'dated'
        };

        $scope.tasker_search = {
            detailed_search: '',
            task_status: '',
            task_sort: 'dated'
        };

        // Star Rating title
        $scope.ratingTitle = ['One', 'Two', 'Three', 'Four', 'Five'];
        // Get Current Location.
        var vm = this;
        $scope.message = 'You can not hide. :)';

        $scope.callbackFunc = function (param) {
            console.log('I know where ' + param + ' are. ' + message);
            console.log('You are at' + $scope.map.getCenter());
            $scope.location_address = $scope.place.formatted_address;
            $scope.data = {
                lat: $scope.place.geometry.location.lat(),
                lng: $scope.place.geometry.location.lng()
            };

        };
        // End ::::::::::

        // Auto Completet Places
        $scope.autocompleteOptions = {
            componentRestrictions: {
                country: 'ca'
            }
        }
        $scope.$on('g-places-autocomplete:select', function (event, place) {
            $scope.data = {
                lat: place.geometry.location.lat(),
                lng: place.geometry.location.lng()
            };
            $scope.map.setCenter(place.geometry.location);
        });

        // Toggle Map on My Task popup
        $scope.toggle = true;
        $scope.toggleMap = function () {
            $scope.toggle = $scope.toggle === false ? true : false;
        };

        $scope.directiontoggle = true;
        $scope.directionToggle = function () {
            $scope.directiontoggle = $scope.directiontoggle === false ? true : false;
        };

        $scope.CurrentDate = new Date();
        $scope.site_url = siteURL;

        if (param != '') {
            MyTasksService.getTaskByID(param).then(function (res) {
                $scope.tasks = res.data['task'][0];
                $scope.images = res.data['images'];
                $scope.site_url = siteURL;
                //console.log($scope.base_url+ ' base_url');
            }, function (err) {
                console.error(err.statusText + ' at ' + err.config.url);
            });

        }


        $scope.task_search_bar = function ($type_data, $type) {
            $timeout.cancel(timer);
            $scope.show = false;
            $scope.page = 0;
            timer = $timeout(function () {
                MyTasksService.getUserTasks(
                    //current_user_id,
                    $scope.user_id,
                    $type_data.detailed_search,
                    $type_data.task_status,
                    $type_data.task_sort,
                    $type).then(function (res) {
                    if ($type == 'employer') {
                        $scope.employerTasks = [];
                        $scope.employerTasks = _.concat($scope.employerTasks, res.data['task_as_employer']);
                        //$scope.employerTasks = res.data['task_as_employer'];
                    } else if ($type == 'tasker') {
                        $scope.taskerTasks = [];
                        $scope.taskerTasks = _.concat($scope.taskerTasks, res.data['task_as_tasker']);
                        //$scope.taskerTasks = res.data['task_as_tasker'];
                    }
                }, function (err) {
                    console.error(err.statusText + ' at ' + err.config.url);
                });

            }, 800);
        };

        $scope.$watch('files', function () {
            $scope.upload($scope.files);
        });

        $scope.$watch('task_img', function () {
            if ($scope.files != null) {
                $scope.files = [$scope.files];

            }
        });

        $scope.log = '';
        $scope.imag_urls = [];

        $scope.upload = function (files) {
            if (files && files.length) {
                for (var i = 0; i < 5; i++) {
                    var file = files[i];
                    if (file) {
                        Upload.upload({
                            url: apiURL + '/upload',
                            data: {
                                file: file
                            }
                        }).then(function (resp) {
                            $timeout(function () {
                                $scope.apiURL = apiURL;
                                $scope.imag_urls.push(resp.data.filename);
                                // $scope.imag_url = apiURL+'/file/'+resp.data.filename;
                                //console.log($scope.imag_url);
                                //console.log(resp.data);
                                //console.log(resp.data.path);
                                $scope.log = 'file: ' +
                                    resp.config.data.file.name +
                                    ', Response: ' + JSON.stringify(resp.data) +
                                    '\n' + $scope.log;
                            });
                        }, null, function (evt) {
                            var progressPercentage = parseInt(100.0 *
                                evt.loaded / evt.total);
                            $scope.log = 'progress: ' + progressPercentage +
                                '% ' + evt.config.data.file.name + '\n' +
                                $scope.log;
                        });
                    }
                }
            }
        };

        // TODO: convert into angular way.
        $scope.clearForm = function () {
            $('.popup-task input, .popup-task textarea').val('');
            $('.popup-task .btn-post-tast').attr("disabled", "disabled");
            $('.popup-task #us21').css("display", "none");
        };

        // TODO: convert into angular way.
        $scope.editTask = function () {
            /*$('.wizard-step1 .btn-post-task').click(function(){
              $('.wizard-step1,.wizard-step2').toggle();
            });*/
            $('.wizard-step1,.wizard-step2').toggle();
        };


        $scope.btn_post_task = function (TaskData) {
            //$scope.post_data = tasks;
            MyTasksService.AddTaskData(TaskData).then(function (res) {
                toastr.success('Task Posted Successfull', '');
                //$window.location.href = siteURL + "/taskercontroller/browse_tasks" ;
                $uibModalInstance.close();
            }, function (err) {
                console.error(err.statusText + ' at ' + err.config.url);
            });
        };

        $scope.master = {};
        $scope.setPopupData = function (tasks) {
            $scope.master = angular.copy(tasks);

            NgMap.getMap().then(function (map) {
                //console.log("getMap on task post");
                $scope.map = google.maps.event.trigger(map, "resize");
            });

            //console.log($scope.master.user_id);
            MyTasksService.GetUserData($scope.master.user_id).then(function (res) {
                $scope.UserData = res.data;
            }, function (err) {
                console.error(err.statusText + ' at ' + err.config.url);
            });

            $('.wizard-step1,.wizard-step2').toggle();
        };

        $scope.form = {}
        $scope.submitForm = function () {
            if ($scope.form.userForm.$valid) {
                console.log('user form is in scope');
                $modalInstance.close('closed');
            } else {
                console.log('userform is not in scope');
            }
        };

        MyTasksService.getUserTasks(
            $scope.user_id,
            $scope.employer_search.detailed_search,
            $scope.employer_search.task_status,
            $scope.employer_search.task_sort,
            'employer').then(function (res) {
            $scope.employerTasks = res.data['task_as_employer'];
            $scope.taskerTasks = res.data['task_as_tasker'];
        }, function (err) {
            console.error(err.statusText + ' at ' + err.config.url);
        });

        $scope.get_task_data = function (task_type, taskid, lat, lng) {
            if (task_type == "employer") {
                $scope.person_type = "Pay";
                $scope.fnt_type = task_type;
            } else {
                $scope.person_type = "Complete";
                $scope.fnt_type = task_type;
            }

            $scope.renderHtml = function (htmlCode) {
                return $sce.trustAsHtml(htmlCode);
            };

            MyTasksService.getTaskByID(taskid).then(function (res) {
                $scope.selected = taskid;
                var task_data = res.data['task'][0];
                var task_rate = task_data.avg_rate;
                $scope.current_user_id = current_user_id;
                $scope.taskdata = res.data['task'][0];
                $scope.taskinfo = res.data['task_info'][0];

                if ($scope.taskinfo != "") {
                    $scope.awarded_to = $scope.taskinfo.awarded_to;
                } else {
                    $scope.awarded_to = "";
                }

                $scope.rate_width = task_rate * 20;

                //Send lat and lng for MAP Direction on My task Popup
                $scope.mapdata = {
                    lat: lat,
                    lng: lng
                };

            }, function (err) {
                console.error(err.statusText + ' at ' + err.config.url);
            });

            MyTasksService.GetNotificationData(taskid, $scope.awarded_to, '2').then(function (res) {
                $scope.NotificationData = res.data;
            });
            $scope.disabledBtn1 = false;
            MyTasksService.checkRatingData($scope.itemClicked, taskid, current_user_id).then(function (res) {
                $scope.NotificationData = res.data;
                if ($scope.NotificationData != "") {
                    $scope.disabledBtn1 = true;
                }
            });


            MyTasksService.checkPaymentData(task_type, taskid, current_user_id).then(function (res) {
                $scope.PaymentData = res.data;
                $scope.payment_type = $scope.PaymentData[0].payment_type;

            });

        }

        $scope.getGame = function (prediction) {
            prediction.gameInfo = {};
            $http.get('/games/' + game_id)
                .success(function (data) {
                    prediction.gameInfo = data;
                });
        };

        //for sending message from mytasks mdoal
        $scope.sendMessage = function (userid, size, parentSelector) {
            // append to body on click
            var parentElem = parentSelector ? angular.element($document[0].querySelector('.global-modal ' + parentSelector)) : undefined;

            var messageModal = $uibModal.open({
                animation: false,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'modal/send_message.html',
                //controller: 'MyTasksController',
                windowClass: 'sendMessage',
                scope: $scope,
                size: size,
                appendTo: parentElem,
                resolve: {}
            });

            messageModal.result.then(function () {
                    console.log('on close event, use if required');
                },
                function () {
                    console.info('Modal dismissed at: ' + new Date());
                });
        };


        //Task completed as a tasker MODEL
        $scope.task_completed_as_tasker = function (userid, size, parentSelector) {
            // append to body on click
            var parentElem = parentSelector ? angular.element($document[0].querySelector('.global-modal ' + parentSelector)) : undefined;

            var messageModal = $uibModal.open({
                animation: false,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'modal/task_completed_as_tasker.html',
                controller: 'TaskCompletedController',
                windowClass: 'task_completed',
                scope: $scope,
                size: size,
                appendTo: parentElem,
                resolve: {}
            });

            messageModal.result.then(function () {
                    console.log('on close event, use if required');
                },
                function () {
                    console.info('Modal dismissed at: ' + new Date());
                });
        };

        //Task completed as a Employer MODEL
        $scope.task_completed_as_employer = function (userid, size, parentSelector) {
            MyTasksService.GetUserData(userid).then(function (res) {
                $scope.UserData = res.data;
            });

            // append to body on click
            var parentElem = parentSelector ? angular.element($document[0].querySelector('.global-modal ' + parentSelector)) : undefined;

            var messageModal = $uibModal.open({
                animation: false,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'modal/task_completed_as_employer.html',
                controller: 'TaskCompletedController',
                windowClass: 'task_completed',
                scope: $scope,
                size: size,
                appendTo: parentElem,
                resolve: {
                    /*UserData: function() {
                   return $scope.UserData;
               }*/
                }
            });

            messageModal.result.then(function () {
                    console.log('on close event, use if required');
                },
                function () {
                    console.info('Modal dismissed at: ' + new Date());
                });
        };


        //Task completed as a Employer MODEL
        $scope.task_completed_fnt = function (person_type, userid, size, parentSelector) {
            MyTasksService.GetUserData(userid).then(function (res) {
                $scope.UserData = res.data;
            });

            // append to body on click
            var parentElem = parentSelector ? angular.element($document[0].querySelector('.global-modal ' + parentSelector)) : undefined;

            var messageModal = $uibModal.open({
                animation: false,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'modal/task_as_' + person_type + '.html',
                controller: 'TaskCompletedController',
                windowClass: 'task_completed',
                scope: $scope,
                size: size,
                appendTo: parentElem,
                resolve: {
                    /*UserData: function() {
                   return $scope.UserData;
               }*/
                }
            });

            messageModal.result.then(function () {
                    console.log('on close event, use if required');
                },
                function () {
                    console.info('Modal dismissed at: ' + new Date());
                });
        };

        $scope.$watchCollection('employer_search', function (newVal, oldVal) {
            $scope.task_search_bar($scope.employer_search, 'employer');
        });
        $scope.$watchCollection('tasker_search', function (newVal, oldVal) {
            $scope.task_search_bar($scope.tasker_search, 'tasker');
        });
    }
})();