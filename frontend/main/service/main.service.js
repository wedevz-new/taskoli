(function(){
    'use strict';
    var app = angular.module('taskoli');
    app.factory('MainService', MainService);

    MainService.$inject = ['$http', 'apiURL', '$uibModal'];

    function MainService($http, apiURL, $uibModal) {
            var service = {
             loginModal: loginModal,
             regModal: regModal
            };
            function loginModal() {
                var instance = $uibModal.open({
                templateUrl: '/frontend/account/view/login.html',
                controller: 'AccountController'
                })
                //todo: send back response
                return instance.result.then(null);
            };
             function regModal() {
                var instance = $uibModal.open({
                templateUrl: '/frontend/account/view/register.html',
                controller: 'AccountController'
                })
                //todo: send back response
                return instance.result.then(null);
            };
            return service;
        }
})();
