(function(){
    'use strict';
    var app = angular.module('taskoli');
    app.controller('MainController', MainController);

    MainController.$inject = ['$scope', '$rootScope', '$sce', 'Upload',  '$uibModal', '$state', '$timeout','siteURL', 'MainService', 'AccountService','$mdSidenav', 'googleMapKey', 'userData'];

    function MainController($scope, $rootScope, $sce, Upload, $uibModal, $state, $timeout, siteURL, MainService, AccountService, $mdSidenav, googleMapKey,userData) {
        var app = this;
        app.user = userData;
        console.log(userData);

        // generating google map url from constants
        $rootScope.trustAsResourceUrl = function () {
            return $sce.trustAsResourceUrl('https://maps.googleapis.com/maps/api/js?libraries=places&key=' + googleMapKey);
        };
       

        // menu from right side
        app.toggleRight = function() {
            $mdSidenav('slide-menu').toggle();
        }

        // main logout
        app.logout = function () {
            AccountService.logout();
            $state.go('logout', {}, { reload: true });
            $timeout(function() {
                $state.go('home');
            }, 2000);
        }
        
        // open login modal
        app.openLogin = function () {
            var login = MainService.loginModal();
            console.log('login')
        }

        // open reg modal
        app.openReg = function () {
            var reg = MainService.regModal();
            console.log('reg')
        }

        // open global mytask
        app.myTasks = function (itemClicked, size, parentSelector) {  
            var itemClicked = itemClicked;
                var mytasksmodal = $uibModal.open({
                animation: false,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/frontend/mytask/view/mytask.html',
                controller: 'MyTasksController',
                windowClass: 'my_task_popup',
                size: 'lg',
                resolve: {
                    'userData': function () {
                    return userData;
                    }
                }
            });

            mytasksmodal.result.then(function (itemClicked) {
                console.log('on close event, use if required');
            }, function () {
                console.info('Modal dismissed at: ' + new Date());
            });
        };

        // open global post task
        app.postTask = function (task_id, size, parentSelector) { 
            var mytasksmodal = $uibModal.open({
                animation: false,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/frontend/posttask/view/post_task.html',
                bindToController:true,
                controller: 'PostTaskController',
                windowClass: 'post_task',
                scope: $scope,
                size: 'lg',
                resolve: {
                    'userData': function () {
                    return userData;
                    },
                    lat: function () {
                        return $scope.lat;
                    },
                    lng: function () {
                        return $scope.lng;
                    }
                }
            });

            mytasksmodal.result.then(function () {
                console.log('on close event, use if required');
            }, function () {
                console.info('Modal dismissed at: ' + new Date());
            });
        };
    }
})();
