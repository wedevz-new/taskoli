(function(){
    'use strict';
    var app = angular.module('taskoli');
    app.controller('MenuController', MenuController);

    MenuController.$inject = ['$scope', '$mdSidenav'];

    function MenuController($scope, $mdSidenav) {
        $scope.toggleRight = function() {
            $mdSidenav('slide-menu').toggle();
        }
    }

})();
