(function(){
    'use strict';
    var app = angular.module('taskoli');
    app.controller('TaskCompletedController', TaskCompletedController);

    TaskCompletedController.$inject = ['$scope', 'toastr', '$uibModal', '$filter','TaskCompletedService', 'siteURL', 'apiURL', '$timeout', 'NgMap','WizardHandler','$uibModalInstance'];

    function TaskCompletedController($scope, toastr, $uibModal, $filter, TaskCompletedService, siteURL, apiURL, $timeout, NgMap,WizardHandler, $uibModalInstance ) {
        
        $scope.enterValidation = function(){
            return true;
        };

    var current_user_id = angular.element('#current_user_id').val();

    /*TaskCompletedService.GetNotificationData($scope.taskdata.id,$scope.awarded_to,'3').then(function(res) { 
            $scope.NotificationData = res.data;
            if($scope.NotificationData  != ""){
            $scope.Cash = 'Cash';
                 $scope.$watch(function() {
                    return WizardHandler.wizard();
                }, function (wizard) {
                    if (wizard) {
                        wizard.goTo(1);
                    // wizard.done(1);
                    }
                });
            }
        });*/

        $scope.disabledBtn = true;
        TaskCompletedService.checkPaymentData($scope.itemClicked,$scope.taskdata.id,current_user_id).then(function(res) { 
            $scope.NotificationData = res.data;
            $scope.disabledBtn = false;
            if($scope.NotificationData[0]  != "" && $scope.itemClicked == "employer"){
              $scope.payment_type = $scope.NotificationData[0].payment_type;

                var wizard = WizardHandler.wizard();
                if (wizard)
                wizard.goTo(1);

                 TaskCompletedService.checkRatingData($scope.itemClicked,$scope.taskdata.id,current_user_id).then(function(res) { 
                                $scope.NotificationData = res.data;
                                console.log($scope.NotificationData);
                                if($scope.NotificationData  != ""){
                                }
                            });
            }
        });

        $scope.exitStepOne = function() {
        if($scope.taskdata.check_payment){
            $scope.disabledBtn = false;
            return true;
        }
        else if ($scope.taskdata.paymentModel) {
                TaskCompletedService.AddPaymentData($scope.taskdata).then(function(res) { 
                // toastr.success('Task Posted Successfull', '');
                $scope.disabledBtn = false;
            }, function(err) {
               console.error(err.statusText + ' at ' + err.config.url);
          });
              return true;
            } else {
                 console.log("not yet completed step 1");
                return false;

            }
        };

    /*    $scope.exitStepTwo = function() {
            if ($scope.taskdata.rate) {
              return true;
            } else {
                 console.log("not yet completed step 2");
                return false;
            }
        };*/


  /*$timeout(function() {
        $scope.exitStepTwo = function() {
            if ($scope.taskdata.rate) {
              //console.log("done with step 2");
        TaskCompletedService.AddRatingData($scope.taskdata,$scope.itemClicked).then(function(res) { 
            //toastr.success('Task Posted Successfull', '');
             //$window.location.href = siteURL + "/taskercontroller/browse_tasks" ;
            //$uibModalInstance.close();
            }, function(err) {
               console.error(err.statusText + ' at ' + err.config.url);
          });
              return true;
            } else {
                 console.log("not yet completed step 2");
                return false;

            }
        };
    }, 400);*/

    /*    $scope.exitStepThree = function() {
            if ($scope.taskdata.review) {
              console.log("done with step 3");
              return true;
            } else {
                 console.log("not yet completed step 3");
                return false;

            }
        };*/

        /*$scope.exitStep1 = function() {
              return true;
        };*/


        $scope.DoneTask = function(itemClicked,taskdata) {
       TaskCompletedService.AddRatingData(itemClicked,taskdata).then(function(res) { 
            toastr.success('All Done', '');
            $uibModalInstance.dismiss('cancel');
            //$uibModalInstance.close();
            }, function(err) {
               console.error(err.statusText + ' at ' + err.config.url);
               return true;
          });
        };

        
        

        $scope.completeTaskAsTasker = function(task_id,user_id) {
          TaskCompletedService.completedTaskAsTasker(task_id,user_id).then(function(res) {
                toastr.success('Task Completed Notification Send Successfully', '');  
                $uibModalInstance.dismiss('cancel');
                $scope.disabledBtn = true;
            }, function(err) {
               console.error(err.statusText + ' at ' + err.config.url);
          });
        };

        $scope.completeTaskAsEmployer = function(TaskData,task_id,user_id) {
          TaskCompletedService.completedTaskAsEmployer(TaskData).then(function(res) {
            
            }, function(err) {
               console.error(err.statusText + ' at ' + err.config.url);
          });
        };

        
     
    } // END of TaskCompletedController //

})();
