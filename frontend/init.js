(function(){
    'use strict';
    var app = angular.module('taskoli', [
        'ui.router',
        'infinite-scroll', 
        'ui.bootstrap',
        'vcRecaptcha',
        '720kb.socialshare',
        'ngMap',
        'angular-loading-bar',
        'rzModule',
        'angularMoment',
        'toastr',
        'google.places',
        'ngFileUpload',
        'ngMaterialDatePicker',
        'mgo-angular-wizard'
        ]).config(config)
        .run(run);

        config.$inject = ['$locationProvider', '$httpProvider'];

        function config($locationProvider, $httpProvider) {
         $locationProvider.html5Mode(true);
         $httpProvider.interceptors.push('HttpInterceptors');
        }
        run.$inject = ['$rootScope', '$state', '$stateParams'];

        function run($rootScope, $state, $stateParams) {
            $rootScope.$on('$stateChangeStart', function(event, toState, toParams){
                console.log('-------- STATE CHANGE TO: ' + toState.name + ' --------');
            });
        }
})();