(function () {
    'usestrict';

    angular.module('taskoli').config(Tasks);

    Tasks.$inject = ['$stateProvider', '$urlRouterProvider'];

    function Tasks($stateProvider, $urlRouterProvider) {
        // States
        $stateProvider
            .state('browse-tasks', {
                parent: 'app',
                url: '/browse-tasks',
                views: {
                    'content@': {
                        templateUrl: '/frontend/tasks/view/tasks.html'
                    }
                },
                controller: 'TasksController'
            });
    }
})();