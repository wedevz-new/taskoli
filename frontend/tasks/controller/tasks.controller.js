(function () {
	'use strict';
	var app = angular.module('taskoli');

	app.controller('TasksController', TasksController);
	TasksController.$inject = ['$scope', '$uibModal', 'TasksService', 'siteURL', 'apiURL', '$timeout', '$window', '$compile', 'NgMap', 'toastr', 'googleMapKey'];

	function TasksController($scope, $uibModal, TasksService, siteURL, apiURL, $timeout, $window, $compile, NgMap, toastr, googleMapKey) {
		$scope.apiURL = apiURL;
		$scope.pauseLoading = true;
		$scope.search = {
			city_name: '',
			detailed_search: '',
			task_status: 'all',
			price_max: '',
			price_min: '',
			posted: ''
		};

		$timeout(function () {
			console.debug("Showing the map. The google maps api should load now.");
			$scope.pauseLoading = false;
		}, 2000);
		var timer;
		NgMap.getMap().then(function (map) {
			$scope.map = map;
		});

		// Toggle Map on My Task popup
		$scope.toggle = true;
		$scope.toggleSearchBar = function (val) {
			if (val == 'focus') {
				$scope.toggle = false;
				//console.log('Open Search');
			} else {
				$scope.toggle = true;
				//console.log('Close Search');
			}
		};

		// Star Rating title
		$scope.ratingTitle = ['One', 'Two', 'Three', 'Four', 'Five'];

		//Current User Id
		var current_user_id = 23;
		$scope.current_user_id = current_user_id;
		console.log('Current User ID ' + current_user_id);

		// Offer List
		$scope.offersList = {
			templateUrl: 'offers_list.html',
			//title: 'Offers List'
		};
		$scope.hovered = function (hovering) {
			$timeout(function () {
				console.log('update with timeout fired');
				if (hovering.objHovered == true) {
					hovering.popoverOpened2 = true;
				}
			}, 500);
		};

		// Make Offer
		$scope.makerOffer = {
			templateUrl: 'make_offers.html',
			//title: 'Make Your Offer'
		};

		$scope.showInfo = function (event, task) {
			//var range = [];
			try {
				TasksService.getTaskByID(task.id, task.user_id).then(function (res) {
					$scope.disabledBtn = false;
					$scope.trackTaskTitle = 'Track Task';
					$scope.selectedData = res.data;
					if ($scope.selectedData['bids'].length) {
						for (var i = 0; i < $scope.selectedData['bids'].length; i++) {
							if ($scope.selectedData['bids'][i].user_id == current_user_id) {
								$scope.myOffer = $scope.selectedData['bids'][i].user_id;
							} else {
								$scope.myOffer = '';
							}
						}
						console.log($scope.myOffer + ' my offer');
					}

					$scope.tracked_tasks = $scope.selectedData['user_info'][0].track_tasks;
					$scope.active = task.id;
					$scope.map.showInfoWindow('myInfoWindow', 'marker_' + task.id);
					$scope.zoomToIncludeMarkers(task.latitude, task.longitude);
				});



				TasksService.GetUserData(current_user_id).then(function (res) {
					$scope.CurrentUserData = res.data;
					$scope.tracked_tasks = $scope.CurrentUserData[0].track_tasks;
				}, function (err) {
					console.error(err.statusText + ' at ' + err.config.url);
				});
			} catch (err) {
				console.error('Can not find a relative marker for task: ' + task.task_title);
			}
		};

		$scope.zoomToIncludeMarkers = function (lat, long) {
			// TODO:  compelted actual code
			var bounds = 23;
			var latLng = 23;

			bounds.extend(latLng);
			$scope.map.fitBounds(bounds);
			$scope.map.panToBounds(bounds);
			$scope.map.setCenter(latLng.getCenter());
			//$scope.map.setZoom(10);
		};

		//TasksService.getTasks(0).then(function(res) {
		TasksService.getSearchTasks(
			$scope.search.detailed_search,
			$scope.search.task_status,
			$scope.search.city_name,
			$scope.search.price_min,
			$scope.search.price_max,
			$scope.search.posted,
			0).then(function (res) {
			$scope.tasks = res.data;
		}, function (err) {
			console.error(err.statusText + ' at ' + err.config.url);
		});


		$scope.myChangeListener = function () {
			$scope.show = true;
			$timeout.cancel(timer);
			$scope.search_bar()
		};


		$scope.priceSlider = {
			minValue: 0,
			maxValue: 1000,
			options: {
				floor: 0,
				ceil: 1000,
				step: 10,
				translate: function (value) {
					return '$' + value;
				},
				onChange: $scope.myChangeListener
			}
		};


		$scope.broadcast = function () {
			$timeout(function () {
				$scope.$broadcast('reCalcViewDimensions');
			}, 170);
		}

		$scope.distanceSlider = {
			minValue: 0,
			maxValue: 1000,
			options: {
				floor: 0,
				ceil: 1000,
				translate: function (value) {
					return value + " km's";
				},
				onChange: $scope.myChangeListener
			}
		};

		$scope.search_bar = function () {
			$timeout.cancel(timer);
			$scope.show = false;
			$scope.page = 0;
			var price_min = $scope.priceSlider.minValue;
			var price_max = $scope.priceSlider.maxValue;

			timer = $timeout(function () {
				$scope.busyLoadingData = false;
				var city = $scope.search.city_name ? $scope.search.city_name.replace(', Canada', '') : '';
				var task_status = $scope.search.task_status;
				var posted = $scope.search.posted;

				TasksService.getSearchTasks(
					$scope.search.detailed_search,
					task_status,
					city,
					price_min,
					price_max,
					posted,
					$scope.page).then(function (res) {
					$scope.tasks = [];
					$scope.tasks = _.concat($scope.tasks, res.data);
					//$scope.tasks = res.data;
					if (res.data.length == 0) {
						$scope.busyLoadingData = true;
						$scope.page = 0;
					}
				}, function (err) {
					console.error(err.statusText + ' at ' + err.config.url);
				});
			}, 800);
		};

		$scope.loadMore = function () {
			if ($scope.busyLoadingData) return;
			console.log('loading more...');
			//TasksService.getTasks(page_no+1).then(function(res) {
			var city = $scope.search.city_name ? $scope.search.city_name.replace(', Canada', '') : '';
			var task_status = $scope.search.task_status;
			var price_min = $scope.priceSlider.minValue;
			var price_max = $scope.priceSlider.maxValue;

			TasksService.getSearchTasks(
				$scope.search.detailed_search,
				task_status,
				city,
				price_min,
				price_max,
				$scope.search.posted,
				$scope.page_no).then(function (res) {
				$scope.tasks = _.concat($scope.tasks, res.data);
				//_.concat($scope.tasks, res.data);
				//$scope.tasks = res.data;
				if (res.data.length == 0) {
					$scope.busyLoadingData = true;
					$scope.page_no = 1;
				}

			}, function (err) {
				console.error(err.statusText + ' at ' + err.config.url);
			});
			$scope.page_no += 1;

		};

		// Track Task
		$scope.trackTask = function (taskID) {
			//$scope.post_data = tasks;
			console.log('taskID ' + taskID + ' ' + current_user_id);
			TasksService.TrackTask(taskID, current_user_id).then(function (res) {
				toastr.success('Task Tracked Successfully', '');
				$scope.disabledBtn = true;
				$scope.trackTaskTitle = 'Task Tracked';

			}, function (err) {
				console.error(err.statusText + ' at ' + err.config.url);
			});
		};

		// Ask Question
		$scope.askQuestion = function (taskID, from, taskBy, message) {
			//$scope.post_data = tasks;
			console.log('taskID ' + taskID, 'taskBy ' + taskBy, ' message ' + message);
			TasksService.AskQuestion(taskID, from, taskBy, message).then(function (res) {
				toastr.success('Your Question has been asked', '');
			}, function (err) {
				console.error(err.statusText + ' at ' + err.config.url);
			});
		};

		// Make Offer
		$scope.makeOffer = function (taskID, offerBy, make_offer) {
			//$scope.post_data = tasks;
			//console.log('taskID '+taskID, 'offerBy '+offerBy, ' makeOffer amount '+make_offer['amount'],' makeOffer message '+make_offer['message']);
			TasksService.MakeOffer(taskID, offerBy, make_offer).then(function (res) {
				toastr.success('Offer Submitted Successfully', '');
			}, function (err) {
				console.error(err.statusText + ' at ' + err.config.url);
			});
		};

		// Question ans chat 
		$scope.submitChat = function (taskID, from, task_by, reply_to, chat) {
			//$scope.post_data = tasks;
			//console.log('taskID '+taskID, 'offerBy '+offerBy, ' makeOffer amount '+make_offer['amount'],' makeOffer message '+make_offer['message']);
			//console.log('chat '+chat, );
			TasksService.chatSubmit(taskID, from, task_by, reply_to, chat).then(function (res) {
				toastr.success('Your Reply Submitted Successfully', '');
			}, function (err) {
				console.error(err.statusText + ' at ' + err.config.url);
			});
		};

		$scope.ask_question = function (userid, size, parentSelector) {
			// append to body on click

			var questionModal = $uibModal.open({
				animation: false,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: 'modal/ask_question.html',
				//controller: 'TasksController',
				windowClass: 'askquestion',
				scope: $scope,
				size: size,
				resolve: {}
			});

			questionModal.result.then(function () {
					console.log('on close event, use if required');
				},
				function () {
					console.info('Modal dismissed at: ' + new Date());
				});
		};

		$scope.qs_ans_chat = function (qsID, by_task, task_id, size, parentSelector) {
			// append to body on click
			$scope.QuestionID = qsID;
			$scope.QuestionsTaskID = task_id;
			$scope.taskBy = by_task;
			TasksService.get_chat(qsID).then(function (res) {
				$scope.chat_data = res.data;
			}, function (err) {
				console.error(err.statusText + ' at ' + err.config.url);
			});
			console.log(qsID + " qs ID");

			var questionModal = $uibModal.open({
				animation: false,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: 'modal/qs_ans_chat.html',
				//controller: 'TasksController',
				windowClass: 'qsAnsChat',
				scope: $scope,
				size: size,
				resolve: {}
			});

			questionModal.result.then(function () {
					console.log('on close event, use if required');
				},
				function () {
					console.info('Modal dismissed at: ' + new Date());
				});
		};

		$scope.myTasks = function (itemClicked, size, user_id, parentSelector) {
			$scope.user_id = user_id;
			$scope.itemClicked = itemClicked;
			var mytasksmodal = $uibModal.open({
				animation: false,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: 'modal/my_tasks.html',
				controller: 'MyTasksController',
				windowClass: 'my_task_popup',
				scope: $scope,
				size: size,
				resolve: {
					param: function () {
						return "";
					}
					/*,
					                user_id: function () {
					                    return 113;
					                }*/

				}
			});
			mytasksmodal.result.then(function (itemClicked) {
					console.log('on close event, use if required');
				},
				function () {
					console.info('Modal dismissed at: ' + new Date());
				});
		};

		$scope.$watchCollection('search', function (newVal, oldVal) {
			$scope.search_bar();
		});

		// Clear Search Form
		$scope.clearSearch = function (search) {
			//Even when you use form = {} it does not work
			search.detailed_search = '';
			search.task_status = null;
			$scope.priceSlider.minValue = 0;
			$scope.priceSlider.maxValue = 1000;
			$scope.distanceSlider.minValue = 0;
			$scope.distanceSlider.maxValue = 1000;
			search.posted = null;
			search.city_name = '';
		}

	}
})();