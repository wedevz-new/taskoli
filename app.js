var express = require('express');
var path = require('path');
var http = require('http');
var bodyParser = require('body-parser');
var config = require('./api/config/config');
var db = require('./api/config/db');
var cors = require('cors')
var app = express();
var session = require('express-session');
var passport = require('passport');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.all('/*' ,cors());
app.use(session({
  secret: 'taskoli app',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: true }
}));
app.use(passport.initialize());
app.use(passport.session());

app.use("/content", express.static(path.join(__dirname, 'content')));

app.use("/frontend", express.static(path.join(__dirname, 'frontend')));

app.use("/bower_components", express.static(path.join(__dirname, 'bower_components')));

var routes = require('./api/routes/main');
app.use('/api', routes);


app.get('*', function(req, res){
  res.sendFile(__dirname + '/frontend/index.html');
});

app.listen(config.server.port, function () {
    console.log('Taskoli is live now at', config.server.host + config.server.port);
});

module.exports = app;