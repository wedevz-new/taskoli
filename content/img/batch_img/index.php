<?php include('template/header_home.php'); ?>
<?php

    $cooky=$_COOKIE['remember'];
    if ($cooky!='yes') { // no cookie, so probably the first time here
        //setcookie ('visited', 'yes', time() + 3600); // set visited cookie
    ?>
        <div id="boxes">
            <div id="dialog" class="window">
                <a href="#" class="close agree"><i class="fa fa-times"></i></a>
                <h6 class="text-uppercase">About Us <i class="fa fa-thumb-tack"></i> </h6>
                <br>

                <p style="margin:0"><b>Need a hand with that?</b></br> Task a friendly neighbour to do your errands</br></br> <b>Need some extra cash?</b></br> No problem. Become a tasker, bid on errands, and make money</p>
                <br>
                <div class="checkbox form-group text-right">
                    <input id="remember" class="chkbox" value="1" type="checkbox" name="chk">
                    <label for="remember" style="color:white;">Dont show this box again!</label>

                </div>

            </div>
            <div id="mask"></div>
        </div>
    <?php }?>



    <div class="search-block">
<?php
                                 $attributes = array('class' => 'form-horizontal', 'method' => 'post');
                                 echo form_open('taskercontroller/browse_tasks', $attributes);
                                 echo validation_errors();
?>
                                <div class="search_input">
                                    <input type="text" name="detailed_search" class="form-control" placeholder="Search for..." />
                                </div>
                                <div class="search_states">
                                    <input type="text" name="location" class="form-control state_item" placeholder="Pick a Location" value="" />
                                    <span id="searchclear" class="searchclear fa fa-times"></span>
                                </div>
                                <div class="search_button">
                                    <button type="submit" class="btn btn-primary btn-block">Browse Tasks</button>
                                </div>
                            <?php echo form_close(); ?>

                            <div class="search_dropdown" >
                                <ul class="regions">
                                    <div class="dropdown_header">
                                        <p>Choose a region:</p>
                                    </div>
                                    <li><a class="victoria" href="#">Vancouver Island</a></li>
                                    <li><a class="vancouver" href="#">Lower Mainland</a></li>
                                </ul>
                                <div id="victoria" class="states_list">
                                    <div class="dropdown_header">
                                        <a href="#" class="back"><i class="fa fa-caret-left"></i> British Columbia</a>
                                    </div>
                                    <ul>
                                        <li>Campbell River</li>
                                        <li>Courtney</li>
                                        <li>Colwood</li>
                                        <li>Duncan</li>
                                        <li>Langford</li>
                                        <li>Nanaimo</li>
                                        <li>Parksville</li>
                                        <li>Port Alberni</li>
                                        <li>Port Hardy</li>
                                        <li>Port Renfrew</li>
                                        <li>Sooke</li>
                                        <li>Sydney</li>
                                        <li>Tofino</li>
                                        <li>Ucluelet</li>
                                        <li>Victoria</li>
                                    </ul>
                                </div>
                                <div id="vancouver" class="states_list">
                                    <div class="dropdown_header">
                                        <a href="#" class="back"><i class="fa fa-caret-left"></i> British Columbia</a>
                                    </div>
                                    <ul>
                                        <li>Abbotsford</li>
                                        <li>Burnaby</li>
                                        <li>Coquitlam</li>
                                        <li>Chilliwack</li>
                                        <li>Delta</li>
                                        <li>Hope</li>
                                        <li>Kelowna</li>
                                        <li>Langley</li>
                                        <li>Maple Ridge</li>
                                        <li>Mission</li>
                                        <li>New Westminister</li>
                                        <li>Port Moody</li>
                                        <li>Richmond</li>
                                        <li>Squamish</li>
                                        <li>Surrey</li>
                                        <li>Vernon</li>
                                        <li>Vancouver</li>
                                        <li>White Rock</li>
                                        <li>Whistler</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <p class="text-center category_text"><b>or browse by category</b></p>
                        <div class="search_categories">
                            <a href="<?php echo base_url('taskercontroller/type/office');?>" class="category_box">
                                <img src="<?php echo base_url('assets/img');?>/icons/pen52.png" />
                                <p>Office</p>
                            </a>
                            <a href="<?php echo base_url('taskercontroller/type/general_labour');?>" class="category_box">
                                <img src="<?php echo base_url('assets/img');?>/icons/logistics9.png" />
                                <p>General Labour</p>
                            </a>
                            <a href="<?php echo base_url('taskercontroller/type/skilled_trades');?>" class="category_box">
                                <img src="<?php echo base_url('assets/img');?>/icons/phone366.png" />
                                <p>Skilled Trades</p>
                            </a>
                            <a href="<?php echo base_url('taskercontroller/type/transportation');?>" class="category_box">
                                <img src="<?php echo base_url('assets/img');?>/icons/logistics33.png" />
                                <p>Transportation</p>
                            </a>
                            <a href="<?php echo base_url('taskercontroller/type/automotive');?>" class="category_box">
                                <img src="<?php echo base_url('assets/img');?>/icons/logistics14.png" />
                                <p>Automotive</p>
                            </a>
                            <a href="<?php echo base_url('taskercontroller/type/household');?>" class="category_box">
                                <img src="<?php echo base_url('assets/img');?>/icons/home134.png" />
                                <p>Household</p>
                            </a>
                            <a href="<?php echo base_url('taskercontroller/type/yardwork');?>" class="category_box">
                                <img src="<?php echo base_url('assets/img');?>/icons/wheelbarrow6.png" />
                                <p>Yardwork</p>
                            </a>
                            <a href="<?php echo base_url('taskercontroller/type/quicktask');?>" class="category_box">
                                <img src="<?php echo base_url('assets/img');?>/icons/notebook83.png" />
                                <p>Quicktask</p>
                            </a>
                        </div>
                        <p>&nbsp;</p>
                        <div class="col-md-12 text-center">
                            <p class="category_text"><b><u>or you might want to</u></b></p>
                            <?php if ($this->session->userdata('logged_in')){ ?>
                                <a href="<?php echo base_url('task_post'); ?>" class="btn btn-primary btn-lg">Post a Task</a>
                            <?php }else{ ?>
                                <a href="#" class="btn btn-primary btn-lg login_popup">Post a Task</a>
                            <?php } ?>
                        </div>
                    </div>

<div class=" pop-up col-md-2">
<div class="menu">
<ul>
<li> &nbsp; <button type="button" class="close" id="close"><i class="fa fa-times" aria-hidden="true"></i></button></li>
<li> <a><i class="fa fa-info-circle" aria-hidden="true"></i></i>FAQ</a></li>
<li> <a><i class="fa fa-cogs" aria-hidden="true"></i></i>HOW IT WORKS</a></li>
<li> <a><i class="fa fa-arrow-right" aria-hidden="true"></i>Home</a></li>
</ul>
</div>
</div>
                </div>
            </div>
        </div>

    </header>

    <!-- Popups Start -->
    <div id="login-wrap" class="popup-wrap">
        <div class="popup-overlay"></div>
        
        <?php
            $login_form_status = $setting_data[0]['login_form'];
            $by_facebook_status = $setting_data[0]['login_facebook'];
            $by_google_status = $setting_data[0]['login_google'];
        ?>
        
        <?php if($by_facebook_status==1 && $by_google_status==1){?>
        
            <div class="popup-box" style="max-height: 200px;">
                <div class="close"><i class="icon icon-cross"></i></div>
                <a href="<?php echo $this->facebook->getLoginUrl(array(
                    'redirect_uri' => site_url('taskercontroller/login_fb'),
                    'scope' => array("email") // permissions here
                ));?>" class="btn btn-facebook btn-block">Login with Facebook</a>
                <a href="<?php echo $this->google->get_login_url();?>" class="btn btn-google btn-block">Login with Google+</a>
                <br>
                Don't have an account? <a href="#" class="signup_popup">Sign Up</a>
            </div>    
                
        <?php }?>
        
        <?php if($by_facebook_status==1 && $by_google_status==0){?>
        
            <div class="popup-box" style="max-height: 160px;min-height: 160px;">
                <div class="close"><i class="icon icon-cross"></i></div>
                <a href="<?php echo $this->facebook->getLoginUrl(array(
                    'redirect_uri' => site_url('taskercontroller/login_fb'),
                    'scope' => array("email") // permissions here
                ));?>" class="btn btn-facebook btn-block">Login with Facebook</a>
                <br>
                Don't have an account? <a href="#" class="signup_popup">Sign Up</a>
            </div>    
                
        <?php }?>
        
        <?php if($by_google_status==1 && $by_facebook_status==0){?>
        
            <div class="popup-box" style="max-height: 160px;min-height: 160px;">
                <div class="close"><i class="icon icon-cross"></i></div>
               
                <a href="<?php echo $this->google->get_login_url();?>" class="btn btn-google btn-block">Login with Google+</a>
                <br>
                Don't have an account? <a href="#" class="signup_popup">Sign Up</a>
            </div>    
                
        <?php }?>
        
        <?php if($login_form_status==1){?>        
        
        <?php if($by_google_status==0 && $by_facebook_status==0){?>
        <div class="popup-box" style="max-height: 330px; padding: 1em 2em;">
        <?php }else{?>
        <div class="popup-box" style="max-height: 435px;">    
        <?php }?>    
            
            <div class="close"><i class="icon icon-cross"></i></div>
            
            <?php if($by_facebook_status==1 && $by_google_status==0){?>
                <a href="<?php echo $this->facebook->getLoginUrl(array(
                    'redirect_uri' => site_url('taskercontroller/login_fb'),
                    'scope' => array("email") // permissions here
                ));?>" class="btn btn-facebook btn-block">Login with Facebook</a>
            <?php }?>
            
            <?php if($by_google_status==1 && $by_facebook_status==0){?>
                <a href="<?php echo $this->google->get_login_url();?>" class="btn btn-google btn-block">Login with Google+</a>
            <?php }?>
            
            <?php if($by_facebook_status==1 && $by_google_status==1){?>
                <a href="<?php echo $this->facebook->getLoginUrl(array(
                    'redirect_uri' => site_url('taskercontroller/login_fb'),
                    'scope' => array("email") // permissions here
                ));?>" class="btn btn-facebook btn-block">Login with Facebook</a>
            
                <a href="<?php echo $this->google->get_login_url();?>" class="btn btn-google btn-block">Login with Google+</a>
            <?php }?>
            
            <span class="or"></span>

            <?php
             $attributes = array('class' => 'form-horizontal', 'method' => 'post');
             echo form_open('login', $attributes);
             echo validation_errors();
            ?>
<!--            <form class="form-horizontal">-->
<!--
                <p>Login as:</p>
                <select class="form-control" id="account_type" name="account_type">
                    <option value="1">Tasker</option>
                    <option value="2">Taskee</option>
                </select>
-->
                <div class="group">
                    <input type="email" name="email" required><span class="highlight"></span><span class="bar"></span>
                    <label>Email</label>
                </div>
                <div class="group">
                    <input type="password" name="password" required><span class="highlight"></span><span class="bar"></span>
                    <label>Password</label>
                    <br>
                    <div class="checkbox">
<!--
                        <input id="remember" type="checkbox">
                        <label for="remember">Remember Me</label>
-->
                        <a href="<?php echo base_url('forgotpassword');?>" class="forgot_pass">Forgot your password?</a>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Login</button>

<!--            </form>-->
        
            <br>
            Don't have an account? <a href="#" class="signup_popup">Sign Up</a>
            <?php echo form_close(); ?>
        </div>
        <?php }?>
        </div>
        
    </div>

    <script type="text/javascript">
        window.onload = function () {
            document.getElementById("password").onchange = validatePassword;
            document.getElementById("confirm_password").onchange = validatePassword;
        }
        function validatePassword(){
        var pass2=document.getElementById("confirm_password").value;
        var pass1=document.getElementById("password").value;
        if(pass1!=pass2)
            document.getElementById("confirm_password").setCustomValidity("Passwords Don't Match");
        else
            document.getElementById("confirm_password").setCustomValidity('');
        //empty string means no validation error
    }
    </script>
    
    <?php
        $signup_form_status = $setting_data[0]['signup_form'];
        $signup_facebook_status = $setting_data[0]['signup_facebook'];
        $signup_google_status = $setting_data[0]['signup_google'];
    ?>

    <div id="signup-wrap" class="popup-wrap">
        <div class="popup-overlay"></div>
        
        <?php if($signup_facebook_status==1 && $signup_google_status==1){?>
        
            <div class="popup-box" style="max-height: 200px;">
                <div class="close"><i class="icon icon-cross"></i></div>
                <a href="<?php echo $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('taskercontroller/signup_fb'),
                'scope' => array("email") // permissions here
                ));?>" class="btn btn-facebook btn-block" title="Facebook Connect">Sign up with Facebook</a>
                
                <a href="<?php echo $this->google->get_login_url();?>" class="btn btn-google btn-block">Sign up with Google+</a>
                <br>
                Don't have an account? <a href="#" class="signup_popup">Sign Up</a>
            </div>    
                
        <?php }?>
        
        <?php if($signup_facebook_status==1 && $signup_google_status==0){?>
        
            <div class="popup-box" style="max-height: 160px;min-height: 160px;">
                <div class="close"><i class="icon icon-cross"></i></div>
                <a href="<?php echo $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('taskercontroller/signup_fb'),
                'scope' => array("email") // permissions here
                ));?>" class="btn btn-facebook btn-block" title="Facebook Connect">Sign up with Facebook</a>
                <br>
                Don't have an account? <a href="#" class="signup_popup">Sign Up</a>
            </div>    
                
        <?php }?>
        
        <?php if($signup_google_status==1 && $signup_facebook_status==0){?>
        
            <div class="popup-box" style="max-height: 160px;min-height: 160px;">
                <div class="close"><i class="icon icon-cross"></i></div>
               
                <a href="<?php echo $this->google->get_login_url();?>" class="btn btn-google btn-block">Sign up with Google+</a>
                <br>
                Don't have an account? <a href="#" class="signup_popup">Sign Up</a>
            </div>    
                
        <?php }?>
        
        
        <?php if($signup_form_status==1){?> 
        
        <?php if($signup_facebook_status==0 && $signup_google_status==0){?>
        <div class="popup-box" style="max-height: 480px; padding: 1em 2em;">
        <?php }else{?>
        <div class="popup-box" style="max-height: 600px;">    
        <?php }?> 
        
            <div class="close"><i class="icon icon-cross"></i></div>
            
            <?php if($signup_facebook_status==1 && $signup_google_status==0){?>
                <a href="<?php echo $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('taskercontroller/signup_fb'),
                'scope' => array("email") // permissions here
                ));?>" class="btn btn-facebook btn-block" title="Facebook Connect">Sign up with Facebook</a>
            <?php }?>
            
            <?php if($signup_google_status==1 && $signup_facebook_status==0){?>
                <a href="<?php echo $this->google->get_login_url();?>" class="btn btn-google btn-block">Sign up with Google+</a>
            <?php }?>
            
            <?php if($signup_facebook_status==1 && $signup_google_status==1){?>
                <a href="<?php echo $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('taskercontroller/signup_fb'),
                'scope' => array("email") // permissions here
                ));?>" class="btn btn-facebook btn-block" title="Facebook Connect">Sign up with Facebook</a>
            
                <a href="<?php echo $this->google->get_login_url();?>" class="btn btn-google btn-block">Sign up with Google+</a>
            <?php }?>
            
           <span class="or1"></span>


            <?php
             $attributes = array('class' => 'form-horizontal', 'method' => 'post');
             echo form_open('signup', $attributes);
             echo validation_errors();
            ?>
            <form class="form-horizontal">
                <div class="group">
                    <input type="text" name="fullname" required><span class="highlight"></span><span class="bar"></span>
                    <label>Full Name</label>
                </div>
                <div class="group">
                    <input type="email" name="email" required><span class="highlight"></span><span class="bar"></span>
                    <label>Email</label>
                </div>
                <div class="group">
                    <input type="password" name="password" id="password" required><span class="highlight"></span><span class="bar"></span>
                    <label>Password</label>
                </div>
                <div class="group">
                    <input type="password" name="confirm_password" id="confirm_password" required><span class="highlight"></span><span class="bar"></span>
                    <label>Confirm Password</label>
                </div>
                <div class="group">
                  <select name="location" class="region" required="required">
                                                  <option selected="selected" value="Vancouver">Vancouver</option>
                                                  <option value="West Vancouver">West Vancouver</option>
                                                  <option value="Surrey">Surrey</option>
                                                  <option value="Delta">Delta</option>
                                                  <option value="White Rock">White Rock</option>
                                                  <option value="Abbotsford">Abbotsford</option>
                                                  <option value="Mission">Mission</option>
                                                  <option value="Richmond">Richmond</option>
                                                  <option value="Burnaby">Burnaby</option>
                                                  <option value="Langley">Langley</option>
                                                  <option value="New_Westminister">New Westminister</option>
                                                  <option value="Port Moody">Port Moody</option>
                                                  <option value="Maple Ridge">Maple Ridge</option>
                                                  <option value="Chiloptionwach">Chiloptionwach</option>
                                                  <option value="Coquitlam"> Coquitlam</option>
                                                  <option value="Hope">Hope</option>
                                                  <option value="Squamish">Squamish</option>
                                                  <option value="Whistler">Whistler</option>
                                                  <option value="Vernon">Vernon</option>
                                                  <option value="Kelowna">Kelowna</option>
                                                  <option disabled="" value="-2">Region: Victoria B.C.</option>
                                                  <option value="Sydney">Sydney</option>
                                                  <option value="Sooke">Sooke</option>
                                                  <option value="Port_Renfrew">Port Renfrew</option>
                                                  <option value="Duncan">Duncan</option>
                                                  <option value="Nanaimo">Nanaimo</option>
                                                  <option value="Parksville">Parksville</option>
                                                  <option value="Port_Abberni">Port Abberni</option>
                                                  <option value="Ucluelet">Ucluelet</option>
                                                  <option value="Tofino">Tofino</option>
                                                  <option value="Courtney">Courtney</option>
                                                  <option value="Campbell_River">Campbell River</option>
                                                  <option value="Port Hardy">Port Hardy</option>
                                              </select>

                </div>
                <button type="submit" class="btn btn-primary btn-block">Sign Up</button>
                <br>
                By signing up, I agree to Taskoly's <a href="<?php echo base_url('pages/tos');?>">Terms of Service</a> and <a href="<?php echo base_url('pages/privacy');?>">Privacy Policy</a>.
            </form>
            <?php echo form_close(); ?>

            
        </div>
        <?php }?>
        </div>
    </div>
    <!-- Popups End -->
        

    <!-- Page Content -->
    <section class="page-content bg-shade">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
<!--
                    <h3>Recently added</h3>
                    <p>Check out new tasks near you!</p>
-->
                    <ul class="tab" style="display: inline-block;width: 24%;">
                      <li><a class="tablinks" onclick="openTask(event, 'open')">Open</a></li>
                      <li><a  class="tablinks" onclick="openTask(event, 'tasked')">Tasked</a></li>
                      <li><a  class="tablinks" onclick="openTask(event, 'completed')">Completed</a></li>
                    </ul>
                    
                    <br><br>
                </div>
                <?php

                    function limit_words($string, $word_limit){
                        $words = explode(" ",$string);
                        return implode(" ",array_splice($words,0,$word_limit));
                    }
                ?>
                
                
                <div id="opens" class="tabcontent" style="display: block !important;">
                    <?php
                    foreach(array_slice($tasks, 0, 9) as $task ){ ?>
                        <div class="col-md-4 col-sm-6">

                            <div class="recently_block">

                                <div class="recently_photo">

                                    <?php if($task->feature_image){?><img src="<?php echo base_url('/assets/img/task_images/'. $task->feature_image)?>" />
                                    <?php }elseif($task->feature_image==''){ ?>
                                        <?php if($task->task_type=='office'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type11.png'); ?>" />
                                        <?php }elseif($task->task_type=='general_labour'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type12.png'); ?>" />
                                        <?php }elseif($task->task_type=='skilled_trades'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type13.png'); ?>" />
                                        <?php }elseif($task->task_type=='transportation'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type14.png'); ?>" />
                                        <?php }elseif($task->task_type=='automotive'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type15.png'); ?>" />
                                        <?php }elseif($task->task_type=='household'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type16.png'); ?>" />
                                        <?php }elseif($task->task_type=='yardwork'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type17.png'); ?>" />
                                        <?php }elseif($task->task_type=='quicktask'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type18.png'); ?>" />
                                        <?php } ?>
                                    <?php } ?>

                                    <div class="recently_media">
                                        <ul class="list-unstyled">
                                            <li><a  onclick="javascript:window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>','',
          'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
          return false;" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>"><img src="<?php echo base_url('assets/img');?>/social/Facebook.png" /></a></li>
                                            <li><a  onclick="javascript:window.open('https://twitter.com/home?status=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>','',
          'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
          return false;" href="https://twitter.com/home?status=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>"><img src="<?php echo base_url('assets/img');?>/social/Twitter.png" /></a></li>
                                            <li><a   onclick="javascript:window.open('https://plus.google.com/share?url=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>','',
          'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
          return false;" href="https://plus.google.com/share?url=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>"><img src="<?php echo base_url('assets/img');?>/social/Google.png" /></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <a href="<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>">
                                <div class="recently_details">
                                    <p><b>Title: </b><?php echo $task->task_title;?><br>
                                    <b>Description: </b><?php echo limit_words($task->task_description,25) . ' ...';?></p>
                                </div>

                                <div class="recently_actions clearfix">
                                    <span class="recently_price"><?php echo '$ '.$task->task_budget;?></span>

                                        <span class="btn btn-default">Check it out</span>

                                </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <div id="tasked" class="tabcontent">
                    <?php
                    foreach(array_slice($tasked, 0, 9) as $task ){ ?>
                        <div class="col-md-4 col-sm-6">

                            <div class="recently_block">

                                <div class="recently_photo">

                                    <?php if($task->feature_image){?><img src="<?php echo base_url('/assets/img/task_images/'. $task->feature_image)?>" />
                                    <?php }elseif($task->feature_image==''){ ?>
                                        <?php if($task->task_type=='office'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type11.png'); ?>" />
                                        <?php }elseif($task->task_type=='general_labour'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type12.png'); ?>" />
                                        <?php }elseif($task->task_type=='skilled_trades'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type13.png'); ?>" />
                                        <?php }elseif($task->task_type=='transportation'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type14.png'); ?>" />
                                        <?php }elseif($task->task_type=='automotive'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type15.png'); ?>" />
                                        <?php }elseif($task->task_type=='household'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type16.png'); ?>" />
                                        <?php }elseif($task->task_type=='yardwork'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type17.png'); ?>" />
                                        <?php }elseif($task->task_type=='quicktask'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type18.png'); ?>" />
                                        <?php } ?>
                                    <?php } ?>

                                    <div class="recently_media">
                                        <ul class="list-unstyled">
                                            <li><a  onclick="javascript:window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>','',
          'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
          return false;" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>"><img src="<?php echo base_url('assets/img');?>/social/Facebook.png" /></a></li>
                                            <li><a  onclick="javascript:window.open('https://twitter.com/home?status=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>','',
          'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
          return false;" href="https://twitter.com/home?status=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>"><img src="<?php echo base_url('assets/img');?>/social/Twitter.png" /></a></li>
                                            <li><a   onclick="javascript:window.open('https://plus.google.com/share?url=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>','',
          'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
          return false;" href="https://plus.google.com/share?url=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>"><img src="<?php echo base_url('assets/img');?>/social/Google.png" /></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <a href="<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>">
                                <div class="recently_details">
                                    <p><b>Title: </b><?php echo $task->task_title;?><br>
                                    <b>Description: </b><?php echo limit_words($task->task_description,25) . ' ...';?></p>
                                </div>

                                <div class="recently_actions clearfix">
                                    <span class="recently_price"><?php echo '$ '.$task->task_budget;?></span>

                                        <span class="btn btn-default">Check it out</span>

                                </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <div id="completed" class="tabcontent">
                    <?php
                    foreach(array_slice($completed, 0, 9) as $task ){ ?>
                        <div class="col-md-4 col-sm-6">

                            <div class="recently_block">

                                <div class="recently_photo">

                                    <?php if($task->feature_image){?><img src="<?php echo base_url('/assets/img/task_images/'. $task->feature_image)?>" />
                                    <?php }elseif($task->feature_image==''){ ?>
                                        <?php if($task->task_type=='office'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type11.png'); ?>" />
                                        <?php }elseif($task->task_type=='general_labour'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type12.png'); ?>" />
                                        <?php }elseif($task->task_type=='skilled_trades'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type13.png'); ?>" />
                                        <?php }elseif($task->task_type=='transportation'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type14.png'); ?>" />
                                        <?php }elseif($task->task_type=='automotive'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type15.png'); ?>" />
                                        <?php }elseif($task->task_type=='household'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type16.png'); ?>" />
                                        <?php }elseif($task->task_type=='yardwork'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type17.png'); ?>" />
                                        <?php }elseif($task->task_type=='quicktask'){?>
                                        <img src="<?php echo base_url('assets/img/task_images/type18.png'); ?>" />
                                        <?php } ?>
                                    <?php } ?>

                                    <div class="recently_media">
                                        <ul class="list-unstyled">
                                            <li><a  onclick="javascript:window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>','',
          'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
          return false;" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>"><img src="<?php echo base_url('assets/img');?>/social/Facebook.png" /></a></li>
                                            <li><a  onclick="javascript:window.open('https://twitter.com/home?status=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>','',
          'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
          return false;" href="https://twitter.com/home?status=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>"><img src="<?php echo base_url('assets/img');?>/social/Twitter.png" /></a></li>
                                            <li><a   onclick="javascript:window.open('https://plus.google.com/share?url=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>','',
          'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
          return false;" href="https://plus.google.com/share?url=<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>"><img src="<?php echo base_url('assets/img');?>/social/Google.png" /></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <a href="<?php echo base_url('task_overview'); ?>?id=<?php echo $task->id; ?>">
                                <div class="recently_details">
                                    <p><b>Title: </b><?php echo $task->task_title;?><br>
                                    <b>Description: </b><?php echo limit_words($task->task_description,25) . ' ...';?></p>
                                </div>

                                <div class="recently_actions clearfix">
                                    <span class="recently_price"><?php echo '$ '.$task->task_budget;?></span>

                                        <span class="btn btn-default">Check it out</span>

                                </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                
                
                
                
            </div>
            </div>

    </section>

    <section id="how" class="page-content">
        <svg id="slit" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="80" viewBox="0 0 100 100" preserveAspectRatio="none">
            <path id="slitPath2" d="M50 100 C49 80 47 0 40 0 L47 0 Z" />
            <path id="slitPath3" d="M50 100 C51 80 53 0 60 0 L53 0 Z" />
            <path id="slitPath1" d="M47 0 L50 100 L53 0 Z" />
        </svg>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1>How It Works</h1>
                    <p class="lead">Experience how <b>Taskoly</b> works</p>
                </div>
                <p>&nbsp;</p>
                <div class="col-md-6">
                    <!--img src="<?php echo base_url('assets/img');?>/how.svg" width="600" class="img-responsive center-block" />
                    <div class="moving_dot"></div-->
                    <div class="browser_wrapper clearfix">
                        <img class="post-img" src="<?php echo base_url('assets/img');?>/post.gif" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="step">
                        <i class="icon icon-search"></i> <h6 class="text-uppercase"><b>POST DESIRED TASK</b></h6>
                        <p>With our simple setup, posting a task is as easy as 1 - 2 - 3</p>
                    </div>
                    <div class="step">
                        <i class="icon icon-compass"></i> <h6 class="text-uppercase"><b>BROWSE POSSIBLE TASKERS</b></h6>
                        <p>Taskers will be able to bid on your task as soon as you post it.</p>
                    </div>
                    <div class="step">
                        <i class="icon icon-quill"></i> <h6 class="text-uppercase"><b>HIRE</b></h6>
                        <p>Choose a Tasker and relax while things get done!</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="cta" class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3 class="text-uppercase">Browse for services</h3>
                    <br>
                    <a href="<?php echo base_url('browse_tasks');?>" class="btn btn-primary btn-lg">Explore</a>
                </div>
            </div>
        </div>
    </section>

    <section id="testimonials" class="page-content">
        <div class="overlay-black"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div id="owl-demo" class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="testimonials-avatar">
                                <img src="<?php echo base_url('assets/img');?>/testimonials/testimonial_0.jpg"/>
                            </div>
                            <div class="testimonials-body">
                                <p>"I was in a tight situation but Taskoly came through for me, I was able to afford my concert tickets!"</p>
                            </div>
                            <div class="testimonials-footer">
                                <span class="footer-help">~Eddy</span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonials-avatar">
                                <img src="<?php echo base_url('assets/img');?>/testimonials/testimonial_0.jpg"/>
                            </div>
                            <div class="testimonials-body">
                                <p>"I was able to have my home cleaned, while I was picking up my in-laws from the airport. My mother in-law even complimented on how clean and fresh my home was. Thanks Melissa."</p>
                            </div>
                            <div class="testimonials-footer">
                                <span class="footer-help">~Karen</span>
                            </div>
                        </div>
                         <div class="item">
                            <div class="testimonials-avatar">
                                <img src="<?php echo base_url('assets/img');?>/testimonials/testimonial_0.jpg"/>
                            </div>
                            <div class="testimonials-body">
                                <p>"I was so busy while caring for my mother, I hardly had time to clean my home or mow my lawn. Thank you, Sheryl and Greg for the awesome job!"</p>
                            </div>
                            <div class="testimonials-footer">
                                <span class="footer-help">~Michelle</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php include('template/footer.php') ?>
<script>
        var owl = $("#owl-demo");

        owl.owlCarousel({
            items : 1, //10 items above 1000px browser width
            itemsDesktop : [1000,5], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,3], // betweem 900px and 601px
            autoPlay: 5000,
            stopOnHover: true
        });

        $('#remember').click(function(alert){
            $('#boxes').toggle();
            document.cookie="remember=yes";
        });
    </script>

    <script>
        function openTask(evt, taskName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(taskName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>

    <style>
       
        ul.tab {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }

        /* Float the list items side by side */
        ul.tab li {float: left;}

        /* Style the links inside the list items */
        ul.tab li a {
            display: inline-block;
            color: black;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            transition: 0.3s;
            font-size: 17px;
            cursor: pointer;
        }

        /* Change background color of links on hover */
        ul.tab li a:hover {
            background-color: #ddd;
        }

        /* Create an active/current tablink class */
        ul.tab li a:focus, .active {
            background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
/*
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
*/
        }
    </style>
