var config = {
    //url to be used in link generation
    url: 'http://localhost:8000/api',
	
    //server details
    server: {
        host:   '127.0.0.1',
        port:   '8000'
    }
};

module.exports = config;