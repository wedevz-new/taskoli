var express = require('express');
var router = express.Router();

var tasks = require('../tasks/index');
router.use('/', tasks);

var users = require('..//users/index');
router.use('/', users);

var upload = require('../uploader/index');
router.use('/', upload);

var account = require('../account/index');
router.use('/account', account);

module.exports = router;