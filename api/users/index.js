var db = require('../config/db');
var express = require('express');
var router = express.Router();
//var app = express();
var table_name = 'users';

var user_column = "users.id as user_id ,users.name, users.lname, users.email, users.user_description, users.user_image, users.cover_image, users.date, users.location, users.telephone, users.avg_rate, users.track_tasks, users.cash_active, users.stripe_active, users.paypal_active";


router.get("/users",function(req,res){
 db.query('SELECT '+user_column+' from ' + table_name, function(err, results, fields) {
	if (!err){
		res.json(results);
	}
	else{
		res.json({"Error" : true, "Message" : "Error executing MySQL query"});
	} 
   }); 
});

router.get("/user/:id",function(req,res){
 var id = parseInt(req.params.id);
 db.query('SELECT '+user_column+' from '+table_name+' WHERE id = ' + id, function(err, results, fields) {
	if (!err){
		res.json(results);
	}
	else{
		res.json({"Error" : true, "Message" : "Error executing MySQL query"});
	} 
   }); 
});

router.put('/trackTask', function (req, res) {
	console.log(req.body.taskid);
	var userid = req.body.userid;
	db.query('SELECT track_tasks from users WHERE id = ' + userid, function(err, results, fields) {
		//res.json(results[0]['track_tasks']);
		var track_tasks = results[0]['track_tasks'] + ',' + req.body.taskid;
		db.query('UPDATE `users` SET `track_tasks`=? where `id`=?', [track_tasks,userid], function (error, results, fields) {
		  if (error) throw error;
		  res.end(JSON.stringify(results));
		});
    });
});

module.exports = router;