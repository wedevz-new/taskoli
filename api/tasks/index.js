var db = require('../config/db');
var express = require('express');
var router = express.Router();
//var app = express();
var bodyParser = require('body-parser');

//start body-parser configuration
router.use( bodyParser.json() );       // to support JSON-encoded bodies
router.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));


var user_column = "users.id as user_id, users.name, users.lname, users.email, users.user_description, users.user_image, users.cover_image, users.date, users.location, users.telephone, users.avg_rate, users.track_tasks, users.cash_active, users.stripe_active, users.paypal_active";

router.get("/tasks", function(req,res){
	var page =  req.query.page;
	var page_limit = page * 10;
 db.query('SELECT task_post.*,'+user_column+' from task_post JOIN users ON task_post.user_id = users.id WHERE task_post.awarded = 0 order by task_post.task_date LIMIT '+page_limit+', 10', function(err, results, fields) {
	if (!err){
		res.json(results);
	}
	else{
		res.json({"Error" : true, "Message" : "Error executing MySQL query"});
	} 
   }); 
});

router.get("/get_tasks", function(req,res){
	var s =  req.query.s;
	var s_query = '';
	if(s != ''){
		s_query = ' AND (task_post.task_title LIKE "%'+s+'%" OR task_post.task_description LIKE "%'+s+'%" OR users.name LIKE "%'+s+'%" )';
	}
	
	var status =  req.query.status;
	var status_query = '';
	if(status == 'all'){
		 status_query = '';
	}else if(status == 'open'){
		 status_query = 'AND task_post.awarded = 0';
	}else if(status == 'tasked'){
		 status_query = 'AND task_post.awarded = 1' ;
	}else{
		 status_query = 'AND task_post.awarded = 0';
	}
	
	
	var price_min =  req.query.price_min;
	var price_max=  req.query.price_max;
	var price_query = '';
	if(price_min != '' && price_max != ''){
		price_query = ' AND task_post.task_budget BETWEEN '+price_min+' AND '+price_max;
	}
	
	
	var posted =  req.query.posted;
	var posted_query = '';
	if(posted == 'newest'){
		posted_query = ' order by task_post.task_date ASC ';
	}else if(posted == 'oldest'){
		posted_query = ' order by task_post.task_date DESC ';
	}else{
		posted_query = ' order by task_post.id DESC ';
		}
	
	var city =  req.query.city;
	var city_query = '';
	if(city != ''){
		city_query = ' AND task_post.address LIKE "%'+city+'%"';
	}
	
	var price =  req.query.price;
	
	var page =  req.query.page;
	var page_limit = page * 10;

 db.query('SELECT task_post.*,'+user_column+' from task_post JOIN users ON task_post.user_id = users.id WHERE task_post.status = 0 '+status_query+s_query+city_query+price_query+posted_query+' LIMIT '+page_limit+', 10 ', function(err, results, fields) {
	if (!err){
		res.json(results);
	}
	else{
		res.json({"Error" : true, "Message" : "Error executing MySQL query"});
	} 
   }); 
});

router.get("/task/:id",function(req,res){
	var id = parseInt(req.params.id);
	var user_id =  parseInt(req.query.user_id);
	var finalResult = {
		task: [],
		related: [],
		images: [],
		questions: [],
		bids:[],
		user_info:[],
		task_info:[]
	};
	
	db.query('SELECT task_post.*,'+user_column+' from task_post JOIN users ON task_post.user_id = users.id WHERE task_post.id = ' + id, function(err, results, fields) {
		if (!err){
			finalResult.task = (results);
		}
		else{
			finalResult.task = ({"Error" : true, "Message" : "Error executing MySQL query"});
		}  
	});
   
   	db.query('SELECT task_post.*,'+user_column+' from task_post INNER JOIN users ON task_post.user_id = users.id order by rand() limit 5', function(err, results, fields) {
		if (!err){
			finalResult.related = (results);
		}
		else{
			finalResult.related = ({"Error" : true, "Message" : "Error executing MySQL query"});
		} 
	});
	
	db.query('SELECT * from task_images WHERE task_id = ' + id, function(err, results, fields) {
		if (!err){
			finalResult.images = (results);
		}
		else{
			finalResult.images = ({"Error" : true, "Message" : "Error executing MySQL query"});
		}   
	}); 
		
	db.query('SELECT chat.*,'+user_column+' from chat INNER JOIN users ON chat.from_id = users.id WHERE reply_to = 0 AND task_id = ' + id, function(err, results, fields) {
		if (!err){
			finalResult.questions = (results);
		}
		else{
			finalResult.questions =({"Error" : true, "Message" : "Error executing MySQL query"});
		} 
   	}); 
	
	db.query('SELECT bidding.*,'+user_column+' from bidding INNER JOIN users ON bidding.user_id=users.id WHERE task_id = ' + id, function(err, results, fields) {
	if (!err){
		finalResult.bids = (results);
	}
	else{
		finalResult.bids = ({"Error" : true, "Message" : "Error executing MySQL query"});
	} 	
   });

   	db.query('SELECT * from current_tasks WHERE task_id = ' + id, function(err, results, fields) {
	if (!err){
		finalResult.task_info = (results);
	}
	else{
		finalResult.task_info = ({"Error" : true, "Message" : "Error executing MySQL query"});
	} 	
   });
   
    db.query('SELECT '+user_column+' from users WHERE id = ' + user_id, function(err, results, fields) {
	if (!err){
		finalResult.user_info = (results);
	}
	else{
		finalResult.user_info = ({"Error" : true, "Message" : "Error executing MySQL query"});
	} 
		res.status(200).json(finalResult);
   }); 
    
});

router.get("/employer_task/:id",function(req,res){
	var user_id = parseInt(req.params.id);
	//var user_id =  parseInt(req.query.user_id);
	var finalResult = {
		user_info:[],
		task_as_employer: []
	};

	var s =  req.query.s;
	var s_query = '';
	if(s != ''){
		s_query = ' AND (task_post.task_title LIKE "%'+s+'%" OR task_post.task_description LIKE "%'+s+'%" OR users.name LIKE "%'+s+'%" )';
	}

	var status =  req.query.status;
	var status_query = '';
	if(status == 'all'){
		 status_query = '';
	}else if(status == 'open'){
		 status_query = ' AND task_post.awarded = 0';
	}else if(status == 'tasked'){
		 status_query = ' AND task_post.awarded = 1 AND task_post.completed = 0' ;
	}else{
		 status_query = '';
	}


	var sort =  req.query.sort;
	var sort_query = '';
	if(sort == 'price_hight'){
		 sort_query = ' ORDER BY task_post.task_budget ASC';
	}else if(sort == 'price_low'){
		 sort_query = ' ORDER BY task_post.task_budget DESC ';
	}else if(sort == 'dated'){
		 sort_query = ' ORDER BY task_post.task_date DESC  ' ;
	}else{
		 sort_query = ' ORDER BY task_post.task_date DESC ';
	}

	
	db.query('SELECT '+user_column+' from users WHERE id = ' + user_id, function(err, results, fields) {
		if (!err){
			finalResult.user_info = (results);
		}
		else{
			finalResult.user_info = ({"Error" : true, "Message" : "Error executing MySQL query"});
		} 
	});
	
	db.query('SELECT task_post.*,'+user_column+' from task_post JOIN users ON users.id = task_post.user_id WHERE task_post.user_id =' + user_id + status_query + s_query + sort_query, function(err, results, fields) {
		if (!err){
			finalResult.task_as_employer = (results);
		}
		else{
			finalResult.task_as_employer = ({"Error" : true, "Message" : "Error executing MySQL query"});
		} 
		res.status(200).json(finalResult); 
	});

    
});

router.get("/tasker_task/:id",function(req,res){
	var user_id = parseInt(req.params.id);
	//var user_id =  parseInt(req.query.user_id);
	var finalResult = {
		user_info:[],
		task_as_tasker: []
	};

	var s =  req.query.s;
	var s_query = '';
	if(s != ''){
		s_query = ' AND (task_post.task_title LIKE "%'+s+'%" OR task_post.task_description LIKE "%'+s+'%" OR users.name LIKE "%'+s+'%" )';
	}

	var status =  req.query.status;
	var status_query = '';
	if(status == 'offers'){
		 status_query = ' AND task_post.awarded = 0 ';
	}else if(status == 'tasked'){
		 status_query = ' AND task_post.awarded = 1 AND task_post.completed = 0 ';
	}else if(status == 'completed'){
		 status_query = ' AND task_post.completed = 1' ;
	}else{
		 status_query = ' ';
	}

	var sort =  req.query.sort;
	var sort_query = '';
	if(sort == 'price_hight'){
		 sort_query = ' ORDER BY task_post.task_budget ASC';
	}else if(sort == 'price_low'){
		 sort_query = ' ORDER BY task_post.task_budget DESC ';
	}else if(sort == 'dated'){
		 sort_query = ' ORDER BY task_post.task_date DESC  ' ;
	}else{
		 sort_query = ' ORDER BY task_post.task_date DESC ';
	}

	
	db.query('SELECT '+user_column+' from users WHERE id = ' + user_id, function(err, results, fields) {
		if (!err){
			finalResult.user_info = (results);
		}
		else{
			finalResult.user_info = ({"Error" : true, "Message" : "Error executing MySQL query"});
		} 
	});

	db.query('SELECT task_post.*,'+user_column+' from task_post JOIN bidding ON bidding.task_id=task_post.id JOIN current_tasks ON bidding.user_id = current_tasks.awarded_to AND bidding.task_id = current_tasks.task_id JOIN users ON current_tasks.awarded_by = users.id WHERE current_tasks.awarded_to = ' + user_id + status_query + s_query + sort_query, function(err, results, fields) {
		if (!err){
			finalResult.task_as_tasker = (results);
		}
		else{
			finalResult.task_as_tasker = ({"Error" : true, "Message" : "Error executing MySQL query"});
		} 
		res.status(200).json(finalResult); 
	});

    
});

router.get("/get_notifications/:id",function(req,res){
 var id = parseInt(req.params.id);
 var user_id =  parseInt(req.query.user_id);
 var bid_type =  parseInt(req.query.bid_type);
 db.query('SELECT * from notifications WHERE taskid = '+ id +' AND bidid = 0 AND is_this_bid = '+ bid_type +' AND is_hired = '+ user_id, function(err, results, fields) {
	if (!err){
		res.json(results);
	}
	else{
		res.json({"Error" : true, "Message" : "Error executing MySQL query"});
	} 
   }); 
});

router.get("/check_payment/:id",function(req,res){
 var id = parseInt(req.params.id);
 var user_id =  parseInt(req.query.user_id);
 var person_type =  req.query.person_type;

 if(person_type == 'employer'){
	payment_query = ' AND employer_id = ' + user_id;
 }
 else{
	payment_query = ' ';
 }
db.query('SELECT payment.*,'+ user_column +' from payment JOIN users ON payment.employer_id = users.id  WHERE task_id = '+ id + payment_query, function(err, results, fields) {
	if (!err){
		res.json(results);
	}
	else{
		res.json({"Error" : true, "Message" : "Error executing MySQL query"});
	} 
   }); 
});

router.get("/check_rating/:id",function(req,res){
 var id = parseInt(req.params.id);
 var user_id =  parseInt(req.query.user_id);
 var person_type =  req.query.person_type;
  if(person_type == 'employer'){
		var person_type = "2";
   }else{
		var person_type = "1";
   }
 db.query('SELECT * from rating WHERE task_id = '+ id + ' AND role = ' + person_type, function(err, results, fields) {
	if (!err){
		res.json(results);
	}
	else{
		res.json({"Error" : true, "Message" : "Error executing MySQL query"});
	} 
   }); 
});

router.post('/addtask', function (req, res,next) {
  //var postData  =  req.body;
  var imgs_length = req.body.img_name.length;

	var postData  = {
			user_id: req.body.user_id, 
			task_title: req.body.task_title, 
			task_description: req.body.task_description,
			start_date:req.body.start_date,
			task_budget: req.body.task_budget, 
			task_location:req.body.task_location['formatted_address'],
			address: req.body.task_location['formatted_address'],
			latitude:req.body.task_location['geometry']['location']['lat'],
			longitude:req.body.task_location['geometry']['location']['lng'],
			status: 0,
			task_sooner: req.body.task_sooner
		};

	//res.json(req.body.img_name);
	//res.json(req.body.img_name.length);
	//res.json(JSON.stringify(postData));

   db.query('INSERT INTO task_post SET ?', postData, function (error, results, fields) {
	  if (error) throw error;
	for(var k=0; k<imgs_length; k++) {
		var ImgData  = {
				task_id: results.insertId, 
				task_img: req.body.img_name[k], 
		};
		db.query('INSERT INTO task_images SET ?', ImgData, function (error, results, fields) {
		if (error) throw error;
			res.end(JSON.stringify(results));
		});
	}
	});

});

router.put('/taskcompleted', function (req, res) {
  db.query('UPDATE `task_post` SET `completed`=? where `id`=?', ['1' ,req.body.task_id], function (error, results, fields) {
	  if (error) throw error;
	  res.end(JSON.stringify(results));
	});
});

/*router.post('/taskcompleted', function (req, res,next) {
  //var postData  =  req.body;

	var postData  = {
			task_id: req.body.id, 
			employer_id: req.body.user_info_id, 
			tasker_id: req.body.user_id,
			amount:req.body.task_budget,
			payment_type: req.body.paymentModel, 
	};


   db.query('INSERT INTO payment SET ?', postData, function (error, results, fields) {
	  if (error) throw error;
	   res.end(JSON.stringify({success: true}));
	});

});*/

router.post('/addpaymentdata', function (req, res,next) {
  //var postData  =  req.body;
	var postData  = {
			task_id: req.body.id, 
			employer_id: req.body.current_user_id, 
			tasker_id: req.body.awarded_to,
			amount:req.body.task_budget,
			payment_type: req.body.paymentModel, 
	};

   db.query('INSERT INTO payment SET ?', postData, function (error, results, fields) {
	  if (error) throw error;
	  res.end(JSON.stringify({success: true}));
	});

});

router.post('/addratingtdata', function (req, res,next) {
  //var postData  =  req.body;
   var itemClicked =  req.query.itemClicked;
   if(itemClicked == 'employer'){
		var person_type = "2";
   }else{
		var person_type = "1";
   }
	var postData  = {
			task_id: req.body.id, 
			tasker_id: req.body.user_id,
			taskee_id: req.body.awarded_to, 
			rated_to: req.body.awarded_to,
			points:req.body.rate,
			feedback:req.body.review,
			role:person_type
	};

   db.query('INSERT INTO rating SET ?', postData, function (error, results, fields) {
	  if (error) throw error;
	   res.end(JSON.stringify({success: true}));
	});

});

router.put('/tasks', function (req, res) {
   db.query('UPDATE `employee` SET `employee_name`=?,`employee_salary`=?,`employee_age`=? where `id`=?', [req.body.employee_name,req.body.employee_salary, req.body.employee_age, req.body.id], function (error, results, fields) {
	  if (error) throw error;
	  res.end(JSON.stringify(results));
	});
});

router.delete('/tasks', function (req, res) {
  // console.log(req.body);
   db.query('DELETE FROM `employee` WHERE `id`=?', [req.body.id], function (error, results, fields) {
	  if (error) throw error;
	  res.end('Record has been deleted!');
	});
});
 
/*app.post("/addtask",function(req,res){
	var query = "INSERT INTO ??(??,??) VALUES (?,?)";
	var table = ["user_login","user_email","user_password",req.body.email,md5(req.body.password)];
	query = mysql.format(query,table);
	db.query(query,function(err,rows){
		if(err) {
			res.json({"Error" : true, "Message" : "Error executing MySQL query"});
		} else {
			res.json({"Error" : false, "Message" : "User Added !"});
		}
	});
});*/

router.delete("/deltask/:id",function(req,res){
	var id = parseInt(req.params.id);
	db.query('DELETE from task_post WHERE id = ' + id, function(err, results, fields){
		if(err) {
			res.json({"Error" : true, "Message" : "Error executing MySQL query"});
		} else {
			res.json({"Error" : false, "Message" : "Deleted the user with email "+id});
		}
	});
});

//Make an Offer
router.post('/makeOffer', function (req, res,next) {
	var postData  = {
			task_id: req.body.task_id, 
			user_id: req.body.offer_by, 
			bid_amount: req.body.amount,
			bid_description:req.body.message,
			bid_status: '1', 
	};

   db.query('INSERT INTO bidding SET ?', postData, function (error, results, fields) {
	  if (error) throw error;
	  res.end(JSON.stringify(results));
	});

});

//Ask Question
router.post('/askquestion', function (req, res) {
	var postData  = {
			from_id: req.body.from_id, 
			to_id: req.body.by_task, 
			by_task: req.body.by_task,
			reply_to:'0',
			task_id: req.body.task_id,
			message: req.body.message
	};

   db.query('INSERT INTO chat SET ?', postData, function (error, results, fields) {
	  if (error) throw error;
	  res.end(JSON.stringify(results));
	});

});

//Question Chat
router.post('/submitchat', function (req, res) {
	var postData  = {
			from_id: req.body.from, 
			to_id: req.body.task_by, 
			by_task: req.body.task_by,
			reply_to:req.body.reply_to,
			task_id: req.body.task_id,
			message: req.body.chat
	};

   db.query('INSERT INTO chat SET ?', postData, function (error, results, fields) {
	  if (error) throw error;
	  res.end(JSON.stringify(results));
	});

});

// Get Chat according to question
router.get("/questionchat/:id",function(req,res){
	var id = parseInt(req.params.id);
 db.query('SELECT chat.*,'+user_column+' from chat JOIN users ON chat.from_id = users.id WHERE reply_to ='+id, function(err, results, fields) {
	if (!err){
		res.json(results);
	}
	else{
		res.json({"Error" : true, "Message" : "Error executing MySQL query"});
	} 
   }); 
});

module.exports = router;