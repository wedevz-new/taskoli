var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var secret = 'taskoli_app';

router.use(function(req, res, next) {
    var token = req.body.token || req.body.query || req.headers['x-access-token'];
    if(token) {
        //verify token
        jwt.verify(token, secret, function(err, decoded) {
            if(err) {
                res.json({success: false, message: 'Invalid Token'});
            }
            else {
                req.decoded = decoded;
                next();
            }
        });
    }
    else {
        res.json({success: false, message: 'No Token Found'});
    } 
});
//todo /auth/me/
router.post('/me', function(req, res, next) {
    res.send(req.decoded);
});
module.exports = router;