var express = require('express');
var router = express.Router();
var passport = require('../config/facebook')
var jwt = require('jsonwebtoken');
var secret = 'taskoli_app';

passport.serializeUser(function(user, done){
  token = jwt.sign({id: user[0].id, name: user[0].name, email: user[0].email, photo: user[0].user_image}, secret, {expiresIn: '24h'});
  done(null, user);
});

passport.deserializeUser(function(id, done){
  db.query('SELECT * from users where id = ?', id, function (err, results, fields) {
   done(err, resutls);
  });
});

router.get('/facebook', 
  passport.authenticate('facebook', {scope: 'email'}));

router.get('/facebook/callback',
 passport.authenticate('facebook', { failureRedirect: '/login/error' }),
 function(req, res) {
    res.redirect('/facebook/' + token);
});

module.exports = router;