var passport = require('passport'),
  config = require('../../config/config'),
  FacebookStrategy = require('passport-facebook').Strategy,
  db = require('../../config/db'),
  moment = require('moment');


passport.use(new FacebookStrategy({
    clientID: '1498820906816411',
    clientSecret: 'd8c490f90ca65ba171f2bc01cd2bd0a6',
    callbackURL: config.url + "/account/login/facebook/callback",
    profileFields: ['id', 'email', 'name', 'picture.type(large) ']
  },
  function(accessToken, refreshToken, profile, done) {
   db.query('SELECT * from users where email = "' + profile._json.email + '"', function (err, results) {
        if(err) done(err);
        if(results.length > 0){ 
          console.log('User exists, Logging in');
          done(null, results);
        }
        else {
            var user = {
              email: profile._json.email,
              name: profile._json.first_name,
              lname: profile._json.last_name,
              user_image: profile._json.picture.data.url,
              is_varified: 0,
              ip_baned: 0,
              fb_baned: 0,
              cash_active: 1,
              membership: 0,
              date: moment().format('YYYY-MM-DD')
              };
              db.query('INSERT into users SET ?', user, function(err, result) {
                if(err) done(err);
                  user.id = result.insertId;
                  console.log('New User registered');
                  done(null, user);
              });
        }
    });
  }
));

module.exports = passport;