var db = require('../config/db');
var express = require('express');
var router = express.Router();
var multer = require('multer');

var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, './task_img')
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1])
    }
    });
var upload = multer({ //multer settings
        storage: storage
    }).single('file');

router.post('/upload', function(req, res) {
    upload(req,res,function(err){
         if(err){
                res.json({error_code:1,err_desc:err});
                 return;
            }
        res.json(req.file);
        })
    });

router.get('/file/:name', function (req, res, next) {
  var options = {
    root:  './task_img/',
    dotfiles: 'deny',
    headers: {
        'x-timestamp': Date.now(),
        'x-sent': true
    }
  };

  var fileName = req.params.name;
  res.sendFile(fileName, options, function (err) {
    if (err) {
      next(err);
    } else {
      //console.log('Sent:', fileName);
    }
  });

});

module.exports = router;