/*
  This is an EXAMPLE gulpfile.js
  You'll want to change it to match your project.
  Find plugins at https://npmjs.org/browse/keyword/gulpplugin
*/
var gulp = require('gulp'),
    wiredep = require('wiredep').stream,
    inject = require('gulp-inject'),
    uglify = require('gulp-uglify'),
    nodemon = require('gulp-nodemon'),
    addTemplates = require('gulp-angular-templatecache'),
    livereload = require('gulp-livereload'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    clean = require('gulp-clean'),
    runSequence = require('run-sequence');

gulp.task('templates', function (done) {
   gulp.src('bower_components/**/*.html')
        .pipe(addTemplates('templateFile.js', {module: 'taskoli' }))
        .pipe(gulp.dest('./frontend/template/'));
        done();
});
gulp.task('inject-all', function (done) {
    var target = gulp.src('frontend/index.html', { base: "." });
     // It's not necessary to read the files (will speed up things), we're only after their paths:
    var sources = gulp.src(['frontend/init.js', 'frontend/**/*.js', 'content/css/**/*.css'], {read: false}, {starttag: '<!-- inject:{{ext}} -->'});
    target.pipe(inject(sources))
        .pipe(wiredep({}))
        .pipe(gulp.dest('.'));
    done();
});

gulp.task('inject-own', function () {
    var target = gulp.src('public/index.html', { base: "." });
    // It's not necessary to read the files (will speed up things), we're only after their paths:
    var sources = gulp.src(['public/js/*.js', 'public/css/*.css'], {read: false}, {starttag: '<!-- inject:{{ext}} -->'});
    target.pipe(inject(sources))
        .pipe(gulp.dest('.'));
});

// TODO: fix overriding
gulp.task('inject-vendor', function () {
    var target = gulp.src('public/index.html', { base: "." });
    var js = gulp.src(wiredep({}).js, {read: false}, {starttag: '<!-- bower:js -->'});
    var css = gulp.src(wiredep({}).css, {starttag: '<!-- bower:css -->'});
    // It's not necessary to read the files (will speed up things), we're only after their paths:
    target
        .pipe(inject(js.pipe(concat('vendor.js')).pipe(gulp.dest('public/js'))))
        .pipe(inject(css.pipe(concat('vendor.css')).pipe(gulp.dest('public/css'))))
        .pipe(gulp.dest('.'));
});

gulp.task('server',function (){  
    nodemon({
        'script': 'app.js'
    });
});

gulp.task('watch', function () {  
    livereload.listen();
});

gulp.task('scripts', function () {  
  return gulp.src('frontend/**/*.js')
    .pipe(uglify())
    .pipe(concat('main.min.js'))
    .pipe(gulp.dest('public/js'))
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(livereload());
});


gulp.task('test', function () {  
  return gulp.src('frontend/**/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(livereload());
});

gulp.task('vendor-scripts', function () {  
  return gulp.src('frontend/**/*.js')
    .pipe(uglify())
    .pipe(concat('main.min.js'))
    .pipe(gulp.dest('public/js'))
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(livereload());
});

gulp.task('styles', function () {  
  return gulp.src('content/**/*.css')
    .pipe(cleanCSS({debug: true}, function(details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest('public/css'))
    .pipe(livereload());
});

gulp.task('copy', function () {
    gulp.src(['frontend/index.html'])
    .pipe(gulp.dest('public'));
});

gulp.task('images', function () {
    gulp.src('content/img/**')
    .pipe(gulp.dest('public/content/img'));
});

gulp.task('clean', function () {
    return gulp.src('public', {read: false})
		.pipe(clean());
});

gulp.task('build', ['clean'], function (callback) {
     runSequence('copy', 'images', 'styles', 'scripts', 'inject-own', 'inject-vendor', callback);
});  

// TODO: Complete watch with injection for dev env
gulp.task('serve', ['server', 'inject-all' ,'watch']);  

gulp.task('default', ['templates', 'inject-all']);